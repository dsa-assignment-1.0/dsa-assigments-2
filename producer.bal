import ballerinax/kafka;

configurable string groupId = "order-consumers";
configurable string orders = "orders";
configurable string paymentSuccessOrders = "payment-success-orders";
configurable decimal pollingInterval = 1;
configurable string kafkaEndpoint = kafka:DEFAULT_URL

public type Order record {|
    int id;
    string desc;
    PaymentStatus paymentStatus
    int orderId;
    string productName;
    decimal price;
    boolean isValid;
|};
public enum PaymentStatus {
    SUCCESS,
    FAIL
}

// Create a subtype of `kafka:AnydataProducerRecord`.
public type OrderProducerRecord record {|
    *kafka:AnydataProducerRecord;
    Order value;
    int key;
|};
final kafka:ConsumerConfiguration consumerConfigs = {
    groupId: groupId,
    topics: [orders],
    offsetReset: kafka:OFFSET_RESET_EARLIEST,
    pollingInterval
};
kafka:Producer orderProducer = check new (kafka:DEFAULT_URL);

public function main() returns error? {
    OrderProducerRecord producerRecord = {
        key: 1,
        topic: "test-kafka-topic",
        value: {
            orderId: 1,
            productName: "ABC",
            price: 27.5,
            isValid: true
        }
    };
    // Sends the message to the Kafka topic.
    check orderProducer->send(producerRecord);
}
